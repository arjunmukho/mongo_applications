from pymongo import MongoClient
from random import randint
client = MongoClient()
db = client.testunb

brands = ["Adidas", "Nike", "Puma", "Reebok", "UCB", "Lee", "Levi", "Gucci", "Guess"]
sellers= ["Jabong", "YepMe", "Myntra", "Flipkart", "ShopperStop"]
cats= ["shirts", "trousers", "jackets", "belts"]
subcats=["formal", "casual", "semi formal"]
colors=["red", "blue", "black", "green"]

i=1
while i<500000:
	brand =  brands[randint(0,len(brands)-1)]
	price =  randint(0,5000)
	category = cats[randint(0,len(cats)-1)]
	subcategory = subcats[randint(0,len(subcats)-1)]
	title = colors[randint(0, len(colors)-1)]+" "+subcategory+" "+category;
	db.testcollection2.save({"ID":i, "brand":brand, "price": price, "title" : title ,"category":category, "subcategory":subcategory, "dcba" : "fiaf", "ceef" : "cdei", "hbae" : "efca", "begg" : "hcac", "bgbg" : "dfdg", "gbgf" : "ebia", "ibib" : "dcei", "ahhh" : "efhf", "bfde" : "cbbi", "haia" : "dfgg", "ebee" : "hfid", "dach" : "hfbh", "bbga" : "gibc", "ehhh" : "hbbi", "fdbb" : "dgig"}); 
	i=i+1
