from pymongo import MongoClient
from random import randint
client = MongoClient()
db = client.testunb

brands = ["Adidas", "Nike", "Puma", "Reebok", "UCB", "Lee", "Levi", "Gucci", "Guess"]
sellers= ["Jabong", "YepMe", "Myntra", "Flipkart", "ShopperStop"]
cats= ["shirts", "trousers", "jackets", "belts"]
subcats=["formal", "casual", "semi formal"]
colors=["red", "blue", "black", "green"]

i=1
while i<500000:
	brand =  brands[randint(0,len(brands)-1)]
	price =  randint(0,5000)
	category = cats[randint(0,len(cats)-1)]
	subcategory = subcats[randint(0,len(subcats)-1)]
	title = colors[randint(0, len(colors)-1)]+" "+subcategory+" "+category;
	db.test_collection.save({"ID":i, "brand":brand, "price": price, "title" : title ,"category":category, "subcategory":subcategory }); 
	i=i+1
